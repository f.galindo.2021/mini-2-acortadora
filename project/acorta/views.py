from django.shortcuts import render
from django.http import Http404, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from .models import Contents, Counter


# Create your views here.


def is_a_page(url):
    if url.startswith("http://") or url.startswith("https://"):
        result = url
    else:
        result = "https://" + url
    return result


def url_list():
    urls = {}
    db = Contents.objects.all()
    for objeto in db:
        urls[objeto.clave] = objeto.valor
    return urls


def counter(url):
    new_object = Counter.objects.first()
    new_object.count += 1
    new_object.save()
    return new_object.count


def check_if_exist(url):
    try:
        content = Contents.objects.get(valor=url)
        content.delete()
    except Contents.DoesNotExist:
        pass


@csrf_exempt
def index(request):
    if request.method == "POST" and 'url' in request.POST:
        url = request.POST['url']
        if len(url) > 0:
            url = is_a_page(url)
            short_url = request.POST['short_url']
            if len(short_url) == 0:
                short_url = counter(url)
            check_if_exist(short_url)
            contents = Contents(clave=url, valor=short_url)
            contents.save()
    urls = url_list()
    return render(request, 'acorta/formulario.html', {'urls': urls})


def get_content(request, llave):
    try:
        content = Contents.objects.get(valor=llave)
        response = content.clave
    except Contents.DoesNotExist:
        raise Http404("Page not Found")
    return HttpResponseRedirect(response)
