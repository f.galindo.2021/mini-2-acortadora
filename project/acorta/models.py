from django.db import models


# Create your models here.

class Contents(models.Model):
    clave = models.CharField(max_length=128)
    valor = models.TextField()


class Counter(models.Model):
    count = models.IntegerField(default=0)
